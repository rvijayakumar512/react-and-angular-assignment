let movietype1=document.querySelector('#btn1');
let movietype2=document.querySelector('#btn2');
let movietype3=document.querySelector('#btn3');
let movietype4=document.querySelector('#btn4');
let movietype5=document.querySelector('#btn5');

movietype1.addEventListener('click',
function(e)
{
  e.preventDefault();
  console.log(movietype1.value);
  showMovies(movietype1.value);
})
movietype2.addEventListener('click',
function(e)
{
  e.preventDefault();
  console.log(movietype2.value);
  showMovies(movietype2.value);
})
movietype3.addEventListener('click',
function(e)
{
  e.preventDefault();
  console.log(movietype3.value);
  showMovies(movietype3.value);
})
movietype4.addEventListener('click',
function(e)
{
  e.preventDefault();
  console.log(movietype4.value);
  showMovies(movietype4.value);
})
movietype5.addEventListener('click',
function(e)
{
  e.preventDefault();
  console.log(movietype5.value);
  showMovies(movietype5.value);
})
const showMovies =async(movietype)=>{
  var flag=0;
  var count=0;
  if(movietype=="View Favorites"){
  movietype="favourit";
  flag=1;
}
else if(movietype=="Search Movies Upcoming"){
  movietype="movies-coming";
}
else if(movietype=="Search Movies In Theatre"){
  movietype="movies-in-theaters";
}
else if(movietype=="Search Top Rated Movies in India"){
  movietype="top-rated-india";
}
else if(movietype=="Search Top Rated Movies"){
  movietype="top-rated-movies";
}
  fetch("http://localhost:3000/"+movietype).
  then(response=>response.json()).
  then(data=>{
    let resulttable=document.getElementById("movies upcoming");
    resulttable.innerHTML=``;
    let template=``;
    data.forEach(function(movie){
      if(flag==0)
      {
      template+=`
      <table style="border: 1px solid" width="100% table-layout: fixed">
      <colgroup>
         <col style="width: 25%"/>
         <col style="width: 45%"/>
         <col style="width: 30%"/>
         </colgroup>
        <tr >
         <td style="border: 1px solid;text-align:center">${movie.title}</td>
         <td style="border: 1px solid;text-align:center"><img src ="img/${movie.poster}" alt="Movie poster"></td>
         <td style="border: 1px solid;text-align:center"><a href="/detail.html?movietype=${movietype}&id=${movie.id}&moviename=${movie.title}&movieyear=${movie.year}">View Detail</a>
         <br/><br/><br/><br/><button id="btn6" onclick="location.href='/favorite.html?movietype=${movietype}&id=${movie.id}&moviename=${movie.title}&movieyear=${movie.year}'" value ="Add to Favorite">Add to Favorite</button>
         </td>

         </tr>
         </table>
      `
    }
    else {
      template+=`
      <table style="border: 1px solid" width="100% table-layout: fixed">
      <colgroup>
         <col style="width: 25%"/>
         <col style="width: 45%"/>
         <col style="width: 30%"/>
         </colgroup>
        <tr>
         <td style="border: 1px solid;text-align:center">${movie.title}</td>
         <td style="border: 1px solid;text-align:center"><img src ="img/${movie.poster}" alt="Movie poster"></td>
         <td style="border: 1px solid;text-align:center"><a href="/detail.html?movietype=${movietype}&id=${movie.id}&moviename=${movie.title}&movieyear=${movie.year}">View Detail</a>
         <br/><br/><br/><br/><button id="btn7" onclick="location.href='/delete.html?movietype=${movietype}&id=${movie.id}&moviename=${movie.title}&movieyear=${movie.year}'" value ="Remove from Favorite">Delete from Favorite</button>
         </td>

         </tr>
         </table>
      `
    }
    });
    if(template==``)
    {
    resulttable.innerHTML=`<h1 style="text-align: center"> Nothing to Display in Favorite List</h1>
    <h2 style="text-align: center">Add some movies as Favorite to view</h2>
    `;
    }
    else{
      let label=``;
      if(flag==0)
      label=`<br/><h2 style="text-align:center"> Displaying Results for your Search...</h2>`;
      else {
        label=`<br/><h2 style="text-align:center">Your Favorite List are...</h2>`
      }
    resulttable.innerHTML=label+`<br/><br/><table style="border: 1px solid" width="100% table-layout: fixed">
       <colgroup>
       <col style="width: 25%"/>
       <col style="width: 45%"/>
       <col style="width: 30%"/>
       </colgroup>
       <tr>
       <th style="border: 1px solid;text-align:center">Movie Name</th>
       <th style="border: 1px solid;text-align:center">Poster</th>
       <th style="border: 1px solid;text-align:center">Details</th>
       </tr>
       </table>`
       +template;
}
  });
}
