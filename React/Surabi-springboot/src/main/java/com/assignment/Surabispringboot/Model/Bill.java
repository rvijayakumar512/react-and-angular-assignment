package com.assignment.Surabispringboot.Model;

import javax.persistence.*;


//Mapping to database table name bill
//Adding all queries to carry out in this table using named native query
@Entity
@Table (name="Bill")
@NamedNativeQuery(name = "Bill.getBillId", query = "Select * from bill order by id DESC limit 1 ",resultClass=Bill.class)
@NamedNativeQuery(name = "Bill.getTodayBill", query = "Select * from bill where day=?1 and month=?2 and year=?3 ",resultClass=Bill.class)
@NamedNativeQuery(name = "Bill.getTotalSale", query = "Select * from bill where month=?1 ",resultClass=Bill.class)
public class Bill {
	//id is primary key annotation
	//column are normal attributes in sql table 
	@Id
	@Column(name = "id")
	private int id;
	@Column(name ="username")
	private String username;
	@Column(name ="amount")
	private String amount;
	@Column(name ="day")
	private String day;
	@Column(name ="month")
	private String month;
	@Column(name ="year")
	private String year;


	//getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}

	//tostring method
	@Override
	public String toString() {
		return "Bill [id=" + id + ", username=" + username + ", amount=" + amount + ", day=" + day + ", month=" + month
				+ ", year=" + year + "]";
	}

}