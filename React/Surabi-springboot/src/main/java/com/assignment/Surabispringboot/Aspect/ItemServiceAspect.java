package com.assignment.Surabispringboot.Aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect  
@Component  
public class ItemServiceAspect   
{  

	//Defining point cut for storing item addition to cart related information 
	@Pointcut(value= "execution(* com.assignment.Surabispringboot.Service.ItemService.addItem(..)) and args(user)")  
	private void logForAddItem()   
	{   
	}

	//Declaring the AfterThrowing advice that is applied when the method addItem throws error
	//This Exception is called when user has entered wrong sequence number of the item
	@AfterThrowing(value="logForAddItem()",throwing="ex")  
	public void afterThrowingAdvice(JoinPoint joinPoint, Exception ex)  
	{  
		System.out.println("After Throwing exception in method:"+joinPoint.getSignature());  
		System.out.println("Exception is:"+ex.getMessage());  
	}     


}  
