import logo from './logo.svg';
import './App.css';
import React, {Component, useState,useEffect} from 'react';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'

import Logincontainer from './containers/Logincontainer';
import Registercontainer from './containers/Registercontainer';
import Homecontainer from './containers/Homecontainer';
import Cartcontainer from './containers/Cartcontainer';
import Logoutcontainer from './containers/Logoutcontainer';
function App() {
  const [token,setToken] = useState("");
  var temp=localStorage.getItem(token);
  const updateToken = (token:string) => {
    localStorage.setItem('token',token);
    temp=localStorage.getItem(token);
    setToken(token);
  }
  return (
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
    <div>
    <Router>
    <Switch>
				<Route exact path = '/' component = {() => (token)?<Redirect to = '/home'/>:<Logincontainer updateToken = {updateToken}></Logincontainer>} />
				<Route exact path = '/home' component = {Homecontainer} />
        <Route exact path = '/register' component = {Registercontainer} />
        <Route exact path = '/cart' component = {Cartcontainer} />
        <Route exact path = '/logout' component = {()=>(token)?<Logoutcontainer updateToken={updateToken}/>:<Redirect to='/'/>} />
        </Switch>
			</Router>
    </div>
  );
}

export default App;
