import React, {Component} from 'react';
import {Navbar, Nav, NavItem,Container,NavDropdown} from 'react-bootstrap';
import logo from './logo.jpg';
function Header(){
  //const navbar = {backgroundColor: '#F16E10', background-image: linear-gradient(to bottom,#2c4053 0,#2c4053 100%)};
  return(
    <div>
    <Navbar style={{backgroundColor:'#F16E10'}} variant="dark">
      <Container style={{width:'66%', paddingTop:'10px',paddingBottom:'10px'}}>
      <div class="text-center" style={{
        paddingTop:'1px',
        paddingBottom:'1px',
        width: '100%',
        border: 'solid lightgray 5px',
        borderRadius: '8px',
      }}>
      <div class="text-light bg-dark display-5">
          <img
            alt=""
            src={logo}
            width="70"
            height="70"
            className="d-inline-block align-top"
          />
        Online Restaurant Manager
        </div>
        </div>
      </Container>
    </Navbar>
</div>
  );
}
export default Header;
