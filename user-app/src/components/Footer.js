import React, {Component} from 'react';
import {Navbar, Nav, NavItem,Container,NavDropdown} from 'react-bootstrap';
import logo from './logo.jpg';
function Footer(props){
  return(
    <div>
    <Navbar style={{backgroundColor:'#F16E10',marginTop: props.val }} variant="dark">
      <Container style={{width:'20%', paddingTop:'2px',paddingBottom:'2px'}}>
      <div class="text-center" style={{
        paddingTop:'1px',
        paddingBottom:'1px',
        width: '100%',
        border: 'solid lightgray 2px',
        borderRadius: '2px',
      }}>
      <div class="text-light bg-dark display-9">
        @Copyrights 2021
        </div>
        </div>
      </Container>
    </Navbar>
</div>
  );
}
export default Footer;
