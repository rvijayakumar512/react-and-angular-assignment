import React, {Component, useState,useEffect} from 'react';
import { Form, InputGroup, FormGroup, FormControl, ControlLabel,Button} from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Link,
  useHistory,
} from 'react-router-dom'


function Login(props){
  var flag=0;
  const [uname,setuname]=useState('');
  const [upass,setupass]=useState('');
  const handlechange=(e)=>{
    e.preventDefault();
    const name=e.target.name;
    if(name=="username")
    {
    setuname(e.target.value);
    }
    else{
    setupass(e.target.value);
  }
  }

const login=(id,username,password)=>{
  //console.log(props);

  const data= {
          userid: id,
          username: username,
          userpassword: password,
          userrole: "user",
          islogin: 1,
      }
  const result=fetch("http://localhost:8083/admin/updateuser/"+id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    console.log(result);
}



  const handlesubmit=(e)=>{
  flag=0;
  fetch("http://localhost:8083/admin/viewusers").
  then(response=>response.json()).
  then(data=>{
    console.log(data);
    data.forEach((data) => {
      if(data.username==uname && data.userpassword==upass)
      {
        flag=1;
        props.updateToken(data.userid);
        login(data.userid,data.username,data.userpassword);
      }
    });

  });

};
  return(
    <div class="container center_div" style={{width:'50%'}}>
  <Form.Group className="mb-3" controlId="formBasicEmail" >
    <Form.Label>UserName</Form.Label>
    <Form.Control type="text" name="username" value={uname} onChange={handlechange} placeholder="Enter Username" />
  </Form.Group>

  <Form.Group className="mb-3" controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" name="userpass" value={upass} onChange={handlechange} placeholder="Enter Password" />
  </Form.Group>
  <div style={{paddingLeft:'270px'}}>
  <Button variant="primary" type="submit" onClick={handlesubmit}>
    Login
  </Button>
    </div>
  <br/><br/>
  <Link to="/register" rel="noopener noreferrer" style={{marginLeft:'220px'}}>New User? Register here</Link>
</div>
  );
}
export default Login;
//<Link to="/home" rel="noopener noreferrer" className="btn btn-primary" onClick={handlesubmit}>Submit</Link>
