import React, {Component, useState,useEffect} from 'react';
import {Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
function Display(props)
{
  var items=[];
  //const [state,newstate]=useState("");
  const [data, setData] = useState([]);
  const [show,setshow]=useState(false);
  const [showcart,setshowcart]=useState(false);
const Displayveg=()=>{
//  var newid=props.id.toString();
  //console.log(typeof(newid));
  const newid =JSON.parse(window.localStorage.getItem('token'));
  console.log('http://localhost:3000/user/'+ newid + '/display/veg');
  fetch('http://localhost:3000/user/'+ newid + '/display/veg').
  then(response=>response.json()).
  then(data=>{
  //  data.forEach(function(data){
      setData(data);
      //console.log(data);
  //});
  });
  setshow(true);
};


const Displaynonveg=()=>{
//  var newid=props.id.toString();
  //console.log(typeof(newid));
  const newid =JSON.parse(window.localStorage.getItem('token'));
  console.log('http://localhost:3000/user/'+ newid + '/display/nonveg');
  fetch('http://localhost:3000/user/'+ newid + '/display/nonveg').
  then(response=>response.json()).
  then(data=>{
  //  data.forEach(function(data){
      setData(data);
setshow(true);
  });
};


const Displaysoftdrink=()=>{
  const newid =JSON.parse(window.localStorage.getItem('token'));
  console.log('http://localhost:3000/user/'+ newid + '/display/softdrinks');
  fetch('http://localhost:3000/user/'+ newid + '/display/softdrinks').
  then(response=>response.json()).
  then(data=>{
      setData(data);
  });
  setshow(true);
};


const Displaydessert=()=>{
  const newid =JSON.parse(window.localStorage.getItem('token'));
  console.log('http://localhost:3000/user/'+ newid + '/display/dessert');
  fetch('http://localhost:3000/user/'+ newid + '/display/dessert').
  then(response=>response.json()).
  then(data=>{
      setData(data);
  });
  setshow(true);
};

const handleadd=(number)=>{
  setshowcart(true);
  const newid =JSON.parse(window.localStorage.getItem('token'));
  console.log(number);
  console.log(typeof(number));
  const data= [{
      no:number,
    }]
    //  console.log(data);
   const result=fetch("http://localhost:3000/user/"+newid+"/additem", {
       method: 'POST',
       headers: {
         'Content-Type': 'application/json'
       },
       body: JSON.stringify(data)
     });
     console.log(result);
}


  return(
    <div>
    <div class="container">
    <div class="get-quote">
        <div class="row">
        <div class="col">
        </div>
            <div class="col-9">
                <h1 id="quote" style={{textAlign:"center"}}>Restaurant Menu</h1>
            </div>
            <div class="col ">
                <Link to="/cart" rel="noopener noreferrer" className="btn btn-primary" >View Cart</Link>
            </div>
        </div>
    </div>
</div>


    <br/><br/><br/>
    <button class='btn-primary' style={{marginRight:'30px',marginLeft:'340px'}} onClick={Displayveg}>Display Veg</button>
    <button class='btn-primary' style={{marginRight:'30px',marginLeft:'30px'}} onClick={Displaynonveg}>Display Non-veg</button>
    <button class='btn-primary' style={{marginRight:'30px',marginLeft:'30px'}} onClick={Displaysoftdrink}>Display Soft-drinks</button>
    <button class='btn-primary' style={{marginRight:'30px',marginLeft:'30px'}} onClick={Displaydessert}>Display Dessert</button>
    <br/><br/>
{show &&
          <div>
          <br/><br/>
          <Table striped bordered hover variant="dark" style={{width:'80%', margin:'auto'}}>
 <thead>
   <tr>
     <th>Sequence Number</th>
     <th>Dish Name</th>
     <th>Amount</th>
     <th>Add to cart</th>
   </tr>
 </thead>
 {
   data.map((data,index)=>{
     return(
 <tbody>
   <tr>
     <td style={{width:'20%'}}>{data.no}</td>
     <td style={{width:'30%'}}>{data.name}</td>
     <td style={{width:'20%'}}>{data.cost}</td>
     <td style={{width:'10%'}}><button class='btn-info' onClick={handleadd.bind(this, data.no)}>Add to cart</button></td>
   </tr>
 </tbody>
)}
)}
</Table>
          </div>
        }
        <br/><br/>

    </div>

  );
}

export default Display;
