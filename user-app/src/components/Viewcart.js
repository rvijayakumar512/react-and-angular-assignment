import React, {Component, useState,useEffect} from 'react';
import {Table,Modal,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';


function Viewcart(){
  const [cart,setcart]=useState([]);
  const [show,setshow]=useState(false);
  const [back,setback]=useState(true);
  const [generate,setgenerate]=useState(false);
  const newid =JSON.parse(window.localStorage.getItem('token'));
  const handleShow=()=>{
    setback(false);
    setgenerate(false);
    fetch("http://localhost:3000/user/"+newid+"/generatebill").
    then(response=>response.json()).
    then(data=>{
      console.log(data);
    });
    setshow(true);
  }
  const handleClose=()=>{
    setshow(false);
  }
  const handlelogout=()=>{
    localStorage.setItem('token','');
    fetch("http://localhost:3000/user/"+newid+"/logout").
    then(response=>response.json()).
    then(data=>{
      console.log(data);
    });

  }
  fetch("http://localhost:3000/user/"+newid+"/showcart").
  then(response=>response.json()).
  then(data=>{
    console.log(data);
    setcart(data);
    if(data.length>=1)
    setgenerate(true);
  });
var bill=0;
  const handledelete=(number)=>{
    console.log(number);
  }

  const handleorder=()=>{
    var name='';
    var password='';
    fetch("http://localhost:3000/user/"+newid+"/logout").
    then(response=>response.json()).
    then(data=>{
      //console.log(data);
    });
    fetch("http://localhost:3000/admin/viewuser/"+newid).
    then(response=>response.json()).
    then(data=>{
      name=data.username;
      password=data.userpassword;
      console.log(name);
      console.log(password);
      //console.log(data);
      //setcart(data);
    });
    const data={
      name:name,
      password:password,
    }
    const result=fetch("http://localhost:3000/user/login", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      });
  }
  return(


    <div>{back&&
    <Link to="/home" rel="noopener noreferrer" className="btn btn-secondary" style={{marginLeft:'1120px'}}>Back</Link>
}
<br/><br/>
{generate&&
    <Table striped bordered hover variant="dark" style={{width:'80%', margin:'auto'}}>
<thead>
<tr>
<th>Sequence Number</th>
<th>Dish Name</th>
<th>Amount</th>
</tr>
</thead>
{
cart.map((data,index)=>{
  var g=parseFloat(data.cost);
  bill+=g;
  return(
<tbody>
<tr>
<td style={{width:'20%'}}>{data.no}</td>
<td style={{width:'30%'}}>{data.name}</td>
<td style={{width:'20%'}}>{data.cost}</td>
</tr>
</tbody>
)}
)}
</Table>
}
<br/><br/>
{back&&
<Button variant="primary" style={{marginLeft:'600px'}} onClick={handleShow} disabled={!generate}>Generate Bill</Button>
}<Modal show={show} onHide={handleClose}>
        <Modal.Header>
          <Modal.Title>Your Bill is {bill} rupees</Modal.Title>
        </Modal.Header>
        <Modal.Body>We are preparing your Order!! You can now sit back and relax!!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" style={{marginRight:'220px'}} onClick={handleClose}>
            Exit
          </Button>
        </Modal.Footer>
      </Modal>
      <br/>
      {!back&&
        <Link to="/home" rel="noopener noreferrer" className="btn btn-secondary" style={{marginLeft:'580px'}} onClick={handleorder}>Place Another Order</Link>

      }<br/><br/><Link to="/logout" rel="noopener noreferrer" className="btn btn-primary" style={{marginLeft:'620px'}} onClick={handlelogout}>Logout</Link>
    </div>
  );
}
export default Viewcart;
