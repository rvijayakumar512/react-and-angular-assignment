import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes,Router } from '@angular/router';
import {AdminserviceService} from '../../service/adminservice.service';
import {User} from  '../../model/user';
@Component({
  selector: 'app-performcrud',
  templateUrl: './performcrud.component.html',
  styleUrls: ['./performcrud.component.css']
})
export class PerformcrudComponent implements OnInit {
users:User[]=new Array<User>();
showusers:boolean=false;
addusers:boolean=false;
deleteusers:boolean=false;
updateusers:boolean=false;
newuser:User=new User();
  constructor(private service:AdminserviceService, private route:Router) { }

  ngOnInit(): void {
  }
view(){
  this.service.fetchUsers().subscribe(
    data=>{
      this.showusers=true;
      this.addusers=false;
      this.deleteusers=false;
      this.updateusers=false;
      console.log(data);
      this.users=data;
    },
    error=>{
      console.log(error.response);
    }
  )
}
add(){
  this.addusers=true;
    this.showusers=false;
    this.deleteusers=false;
    this.updateusers=false;
  }
  update(){
    this.addusers=false;
      this.showusers=false;
      this.deleteusers=false;
      this.updateusers=true;
  }
  delete(){
    this.addusers=false;
      this.showusers=false;
      this.deleteusers=true;
        this.updateusers=false;
  }
  submit(){
    if(this.addusers==true&&this.updateusers==false&&this.deleteusers==false)
    {
  this.service.addUsers(this.newuser).subscribe(
    data=>{
      console.log("successful");
    },
    error=>{

    }
  )
    this.newuser=new User();
}
else if(this.addusers==false&&this.updateusers==true&&this.deleteusers==false)
{
  this.service.updateUsers(this.newuser).subscribe(
    data=>{
      console.log("successful");
    },
    error=>{

    }
  )
    this.newuser=new User();

}
else if(this.addusers==false&&this.updateusers==false&&this.deleteusers==true){
  this.service.deleteUsers(this.newuser).subscribe(
    data=>{
      console.log("successful");
    },
    error=>{

    }
  )
    this.newuser=new User();
}
}
}
