import { Component, OnInit,Input } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {User} from '../../model/user';
import {LoginserviceService} from '../../service/loginservice.service';
import { RouterModule, Routes,Router } from '@angular/router';

@Component({
  selector: 'app-loginform',
  templateUrl: './loginform.component.html',
  styleUrls: ['./loginform.component.css']
})
export class LoginformComponent implements OnInit {
  user=new User();
  constructor(private service:LoginserviceService,private route:Router) { }

  ngOnInit(): void {

  }
checklogin(){
 this.service.checkforAdmin(this.user).subscribe(
   data=>{
     console.log(data);
     console.log("successful");
     this.user.userid=1;
     localStorage.setItem('id',this.user.userid.toString());
     this.route.navigate(['view']);
   },
   error=>{
     console.log(error.response);
     console.log("not successsful");

   }
 )
}
}
