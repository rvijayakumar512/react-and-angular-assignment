import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../model/user';
@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  constructor(private httpclient:HttpClient) { }

  fetchAllUsers():Observable<any>{
  return  this.httpclient.get<any>("http://localhost:8083/admin/viewusers");
  }

  checkforAdmin(user: User):Observable<any>{
  return  this.httpclient.post<any>("http://localhost:8083/admin/login",user);
  }
}
