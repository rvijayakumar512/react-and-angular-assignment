import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudcontainerComponent } from './crudcontainer.component';

describe('CrudcontainerComponent', () => {
  let component: CrudcontainerComponent;
  let fixture: ComponentFixture<CrudcontainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudcontainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
