import { Component, OnInit } from '@angular/core';
import {LoginserviceService} from '../../service/loginservice.service';
@Component({
  selector: 'app-logincontainer',
  templateUrl: './logincontainer.component.html',
  styleUrls: ['./logincontainer.component.css']
})
export class LogincontainerComponent implements OnInit {
  constructor(private service:LoginserviceService) { }

  ngOnInit(): void {
    this.service.fetchAllUsers().subscribe(
      data=>console.log(data),
      error=>console.log(error.response)
    )
  }

}
