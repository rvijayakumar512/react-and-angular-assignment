
# Angular and React Assignment

## Admin Panel - Angular

### Setting Up Environment
* Clone the git repo.
* admin-app folder contains all the functionalities of admin implemented in Angular JS
* move into the the admin-app folder after cloning.
* Run **npm install** command to install all the required node modules into your local system. 
* Run **ng server --open** to initialise Angular development server.
* Once the server is set up, now int he browser type **http://localhost:4200/**

### Testing Admin functionalities
#### Login
* Admin has no registration. Already in the database it is assumed there is only one admin with
   * username: vijay
   * password: vijay123
* Login with the above credentials to test the functionalities.
* You will be redirected to a view page where you will find three buttons.
#### View Bills
* Click view bill today button to view all bills generated on the current day(Make sure you have entries in database with current date as bill date or generate bill from react user-module and click this button) 
* Select a month from dropdown and click get total sale to view total sale fot that month along with bills contributing to the sale.
* Click on Perform Crud button and you will be redirected to crud operations page where you can view users, add new users, update existing users and delete users.
#### Perform CRUD on Users
* Click view users to view all users.
* Click Add user and enter all the input asked in the page and click add button to add the user to database.(Enter new unique userid i.e which doesnot exist in db)
* You can now click view users and at the bottom of the list you can find the new user which you added now.
* Click update user and enter all the input asked in the page and click update button to update the user in database.(Enter already exisitng userid i.e which exist in db to update the same)
* You can now click view users and in the list displayed you can find the user id and details of user which you updated now.
* Click delete user button and enter the user id you need to delete(Enter exisiting user id i.e present in db to delete it).
* You can now click view users and in the list displayed you will not find the user id and details of user which you entered to delete.
