package com.assignment.Surabispringboot.Service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.Surabispringboot.Controller.ItemController;
import com.assignment.Surabispringboot.Model.Item;
import com.assignment.Surabispringboot.Repository.ItemRepository;

@Service
public class ItemService {

	@Autowired
	ItemRepository itemRepo;
	@Autowired
	ItemController itemcontroller;


	//implementation of display for user -->veg
	public List<Item> displayVeg() 
	{
		List<Item> item= itemRepo.display("veg");
		return item;
	}


	//implementation of display for user -->non veg
	public List<Item> displayNonVeg()
	{
		List<Item> item= itemRepo.display("nonveg");
		return item;
	}


	//implementation of display for user -->soft drinks
	public List<Item> displaySoftdrinks()
	{
		List<Item> item= itemRepo.display("soft drinks");
		return item;
	}


	//implementation of display for user -->dessert
	public List<Item> displayDessert()
	{
		List<Item> item= itemRepo.display("dessert");
		return item;
	}


	//implementation of additem to user cart
	public String addItem(List<Item> item) throws Exception 
	{
		int x=0;
		Iterator it=item.iterator();
		while(it.hasNext())
		{
			Item i=(Item) it.next();
			Item llist=itemRepo.checkdb(i.getNo());
			if(llist!=null) {
				itemcontroller.glist.add(llist);
				//System.out.println(itemcontroller.glist);
			}
			else
			{
				x=1;
				throw new Exception("Invalid sequence number entered for item");
			}
		}
		return "Items added";
	}


	//implementation of view cart for user
	public List<Item> showCart() throws Exception
	{
		if(itemcontroller.glist==null)
			throw new Exception("No items added to cart to show..!");
		else
			return itemcontroller.glist;
	}

}
