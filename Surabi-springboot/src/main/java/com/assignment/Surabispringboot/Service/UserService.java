package com.assignment.Surabispringboot.Service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.Surabispringboot.Controller.ItemController;
import com.assignment.Surabispringboot.Model.User;
import com.assignment.Surabispringboot.Repository.LoginRepository;

@Service
public class UserService {
	@Autowired
	LoginRepository loginRepo;
	
	@Autowired
	ItemController itemcontroller;
	
	//implementation of register user/admin
	public String register(User userDetails)
	{
		String name =userDetails.getUsername();
		String password=userDetails.getUserpassword();
		User user=loginRepo.getUserRegister(name,password);
		if(user!=null)
		{
			return "User with same name and password already Exists....So cannot register user";
		}
		else
		{
			user=new User();
			User u1=loginRepo.getUserId();
			user.setUserid(u1.getUserid()+1);
			user.setUsername(userDetails.getUsername());
			user.setUserpassword(userDetails.getUserpassword());
			user.setUserrole("user");
			user.setIslogin(0);
			loginRepo.save(user);
			return "User Registered Successfully!!";
		}
	}

	//implementation of login for user
	public String userLogin(User user) throws Exception  {
		String username=user.getUsername();
		String userpassword=user.getUserpassword();
		String userrole=user.getUserrole();
		User u=loginRepo.login(username,userpassword,userrole);
		if(u!=null)
		{	
			if(u.getIslogin()==1)
				return "User already logged in";
			else {
			user.setIslogin(1);
			loginRepo.save(user);
			return username+" is loggged in  successfully"+" as "+userrole;
			}
		}
		else {
			throw new Exception ("Invaid credentials!!!");
		}
	}


	
	//implementation of login for admin
	public String adminLogin(User user)
	{
		String username=user.getUsername();
		String userpassword=user.getUserpassword();
		String userrole=user.getUserrole();
		User u=loginRepo.login(username,userpassword,userrole);
		if(u!=null)
		{
			if(u.getIslogin()==1)
				return "Admin already logged in";
			else {
			user.setIslogin(1);
			loginRepo.save(user);
			return username+" is loggged in  successfully"+" as "+userrole;
			}	
		}
		else 
			return "User with given credentials doesnot exists\nLogin Invalid due to bad credentials";
	}


	
	
	//implementation of CURD of admin on user --> read by userid
	public User viewUser(Integer userId)
	{
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		return user;	
	}

	
	
	
	//implementation of CURD of admin on user --> readall
	public List<User> viewAllUser()
	{
		return loginRepo.getAllUser("user");
	}

	
	
	
	//implementation of CURD of admin on user --> create/add
	public String addUser(Integer userId, User userDetails)
	{
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		System.out.println(user);
		if(user!=null)
		{
			return "User with same id already Exists....So cannot add user";
		}
		else
		{
			user=new User();
			user.setUserid(userDetails.getUserid());
			user.setUsername(userDetails.getUsername());
			user.setUserpassword(userDetails.getUserpassword());
			user.setUserrole(userDetails.getUserrole());
			user.setIslogin(userDetails.getIslogin());
			loginRepo.save(user);
			return "User added Successfully!!";
		}
	}



	//implementation of CURD of admin on user --> update
	public String updateUser(Integer userId, User userDetails)
	{
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		if(user!=null)
		{
			user.setUserid(userDetails.getUserid());
			user.setUsername(userDetails.getUsername());
			user.setUserpassword(userDetails.getUserpassword());
			user.setUserrole(userDetails.getUserrole());
			user.setIslogin(userDetails.getIslogin());
			loginRepo.save(user);
			return "User Updated Successfully!!";
		}
		else
			return "User with given id doesnot exist..Try adding as a new user";
	}



	//implementation of CURD of admin on user --> delete
	public String deleteUser(Integer userId)
	{
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		if(user!=null)
		{
			loginRepo.delete(user);
			return "User deleted Successfully!!";
		}
		else
			return "User with same id has been already deleted....So cannot delete user";
	}

	
	
	
	//implementation of logout for user
	public String logoutuser(Integer userId)
	{
		itemcontroller.glist.clear();
		itemcontroller.bill=0;
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		if(user!=null)
		{
			if(user.getIslogin()==1) {
			user.setIslogin(0);
			loginRepo.save(user);
			return "User Logged out Successfully!!";
			}
		else
			return "User already logged out or not logged in at all...";
		}
		else
		{
			return "Invalid userid in URL";
		}
		}
	
	
	
	
	
	//implementation of logout for admin
	public String logoutadmin(Integer userId)
	{
		int i=userId.intValue();
		User user=loginRepo.getUser(i);
		if(user!=null)
		{
			if(user.getIslogin()==1) {
			user.setIslogin(0);
			loginRepo.save(user);
			return "Admin Logged out Successfully!!";
			}
		else
			return "Admin already logged out or not logged in at all...";
		}
		else
		{
			return "Invalid admin id in URL";
		}
		}

}