package com.assignment.Surabispringboot.Aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;


@Aspect  
@Component  
public class UserServiceAspect   
{  
	//Defining pointcut for Storing logging related information
	@Pointcut(value= "execution(* com.assignment.Surabispringboot.Controller.LoginController.*(..)) and args(user)")  
	private void logForLogin()   
	{   
	}

	//Defining pointcut for Storing Exception of logging related information
	@Pointcut(value= "execution(* com.assignment.Surabispringboot.Service.UserService.userLogin(..)) and args(user)")  
	private void logForExceptionInLogin()   
	{   
	}

	//Declaring AfterThrowing advice which gives us the message to our log when user/admin has entered wrong credentials for login
	@AfterThrowing(value="logForExceptionInLogin()",throwing="ex")  
	public void afterThrowingAdvice(JoinPoint joinPoint, Exception ex)  
	{  
		System.out.println("After Throwing exception in method:"+joinPoint.getSignature());  
		System.out.println("Exception is:"+ex.getMessage());  
	}     


	//Declaring the around advice that is applied before and after the method matching with a pointcut expression
	//Applied when user/admin logs in
	//If any exception it is given by AfterThrowing advice
	@Around(value= "logForLogin()")  
	public Object aroundAdvice(ProceedingJoinPoint jp) throws Throwable   
	{  
		String method=jp.getSignature().getName();
		String classname=jp.getTarget().getClass().toString();
		System.out.println("**************************From aspect class before logging in*******************************" + "\nmethod:  "+ method +  "class: " + classname +"\n");  
		Object result = null;
		try   
		{  
			result= jp.proceed();
			System.out.println(result);
		}   
		catch(Exception e)
		{  

		}

		System.out.println("\n**************************From aspect class after logging in*******************************\n\n");
		return result;
	}  
}  
